##Discount processing service example

###Preparing
#####Initialize libraries
clone repository and run  
*`composer` should be already installed*
```
composer install
```

#####Initialize database
set Environment variable **DATABASE_URL**  in **".env"** file
```
DATABASE_URL=mysql://root:123@127.0.0.1:3306/webgears
```

create a database
```
bin/console doctrine:database:create
``` 
create tables
```
bin/console doctrine:schema:create
``` 

#####Configure web server
Create config for Apache/nginx or other web server pointing on `public/` directory as a web root.

As alternative you can start two instances of built-in web server and access it with the one which is not configured for API_URL.  
```
cd public/
php -dvariables_order=EGPCS -S 127.0.0.1:8005 ../vendor/symfony/web-server-bundle/Resources/router.php
php -dvariables_order=EGPCS -S 127.0.0.1:8000 ../vendor/symfony/web-server-bundle/Resources/router.php
```  
*Symfony web server could only run one instance.  
So you should use php built-in server directly to run two instances of web server*

#####Set URL for API
set environment variable **API_URL**  in **".env"** file
```
API_URL=http://127.0.0.1:8000/API/getDiscounts/
```

####Running unit test

PHPUnit tested  could be executed with this command
```
bin/phpunit
```

All test files are located in ``tests/`` directory
 
 

