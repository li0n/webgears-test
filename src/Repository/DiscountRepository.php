<?php

namespace App\Repository;

use App\Entity\Discount;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Discount|null find($id, $lockMode = null, $lockVersion = null)
 * @method Discount|null findOneBy(array $criteria, array $orderBy = null)
 * @method Discount[]    findAll()
 * @method Discount[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DiscountRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Discount::class);
    }

    public function getVisibleDiscountsSortedByFoundDate()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('d');
        $qb->from(Discount::class, 'd');
        $qb->where('d.dateFound > d.dateHidden');
        $qb->orWhere('d.dateHidden is Null');
        $qb->orderBy('d.dateFound');
        return $qb->getQuery()->getResult();
    }

}
