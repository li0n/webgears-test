<?php
namespace App\Interfaces;


use App\Entity\Discount;

interface DiscountServiceInterface
{

    /**
     * @return array|Discount[]
     */
    public function getDiscountsFromAPI();

}