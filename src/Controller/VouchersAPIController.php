<?php
namespace App\Controller;


use App\Service\VouchersAPIService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class VouchersAPIController extends AbstractController
{
    const LOADED_COOKIE_NAME = 'loaded';

    /** @var VouchersAPIService */
    private $vouchersAPIService;

    /**
     * VouchersAPIController constructor.
     * @param VouchersAPIService $vouchersAPIService
     */
    public function __construct(VouchersAPIService $vouchersAPIService)
    {
        $this->vouchersAPIService = $vouchersAPIService;
    }

    /**
     * @Route("/API/getDiscounts/")
     * @param Request $request
     * @return string
     */
    public function getDiscounts(Request $request)
    {
        $isFirstFileLoaded = $request->cookies->has(self::LOADED_COOKIE_NAME);
        $this->getVouchersAPIService()->setIsLoaded($isFirstFileLoaded);
        $response = new JsonResponse($this->getVouchersAPIService()->getDiscounts());
        $response->headers->setCookie(new Cookie(self::LOADED_COOKIE_NAME, 1));
        return $response;
    }

    /**
     * @return VouchersAPIService
     */
    private function getVouchersAPIService(): VouchersAPIService
    {
        return $this->vouchersAPIService;
    }


}