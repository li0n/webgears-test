<?php
namespace App\Controller;


use App\Entity\Discount;
use App\Interfaces\DiscountServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DiscountsController extends AbstractController
{

    /** @var DiscountServiceInterface */
    private $discountService;

    /**
     * DiscountsController constructor.
     * @param DiscountServiceInterface $discountService
     */
    public function __construct(DiscountServiceInterface $discountService)
    {
        $this->discountService = $discountService;
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("/hideDiscount/{id}", name="hideDiscount")
     * @throws \Exception
     */
    public function hideDiscount($id)
    {
        $discount = $this->getDiscountsRepository()->find($id);
        if (!$discount) {
            return new JsonResponse(sprintf('ERROR: Discount with id %d is not found', $id));
        }

        $discount->setDateHidden(new \DateTime());
        $this->getDoctrine()->getManager()->persist($discount);
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse("OK");
    }

    /**
     * @param $discountServiceCookieFile
     * @return Response
     * @Route("/")
     */
    public function index($discountServiceCookieFile)
    {
        file_put_contents($discountServiceCookieFile, ""); // Reset time
        $newDiscounts = $this->getDiscountService()->getDiscountsFromAPI();
        $this->saveDiscounts($newDiscounts);

        $discounts = $this->getDiscountsRepository()->getVisibleDiscountsSortedByFoundDate();
        return $this->render("discounts.html.twig", [
            'isAJAX' => false,
            'discounts' => $discounts
        ]);
    }


    /**
     * @param Request $request
     * @Route("/update", name="updateDiscounts")
     * @return Response
     */
    public function updateDiscounts(Request $request)
    {
        $newDiscounts = $this->getDiscountService()->getDiscountsFromAPI();
        $this->saveDiscounts($newDiscounts);

        $discounts = $this->getDiscountsRepository()->getVisibleDiscountsSortedByFoundDate();
        return $this->render("discounts.html.twig", [
            'isAJAX' => true,
            'discounts' => $discounts,
        ]);
    }

    /**
     * @param array $newDiscounts
     */
    private function saveDiscounts(array $newDiscounts): void
    {
        foreach ($newDiscounts as $discount) {
            $this->getDoctrine()->getManager()->persist($discount);
        }
        $this->getDoctrine()->getManager()->flush();
    }

    private function getDiscountService(): DiscountServiceInterface
    {
        return $this->discountService;
    }

    /**
     * @return \App\Repository\DiscountRepository|\Doctrine\Common\Persistence\ObjectRepository
     */
    private function getDiscountsRepository()
    {
        return $this->getDoctrine()->getRepository(Discount::class);
    }


}