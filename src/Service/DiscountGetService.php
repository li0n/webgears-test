<?php
namespace App\Service;


use Doctrine\ORM\EntityManagerInterface;

class DiscountGetService extends AbstractDiscountService
{
    private $APIURL;
    private $cookieFile;

    /**
     * DiscountService constructor.
     * @param EntityManagerInterface $em
     * @param $APIURL
     * @param $discountServiceCookieFile
     */
    public function __construct(EntityManagerInterface $em, $APIURL, $discountServiceCookieFile)
    {
        parent::__construct($em);
        $this->APIURL = $APIURL;
        $this->cookieFile = $discountServiceCookieFile;
    }

    /**
     * @return array
     */
    protected function loadFromAPI() : array
    {
        $ch = \curl_init();
        \curl_setopt($ch, CURLOPT_URL, $this->APIURL);
        \curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        \curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        \curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookieFile); // Cookie aware
        \curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookieFile); // Cookie aware
        \curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $response = \curl_exec($ch);
        $data = json_decode($response, true);
        return  ($data == null) ? [] : $data;
    }

}