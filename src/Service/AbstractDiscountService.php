<?php
namespace App\Service;


use App\Entity\Discount;
use App\Interfaces\DiscountServiceInterface;
use App\Repository\DiscountRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

abstract class AbstractDiscountService implements DiscountServiceInterface
{

    /** @var EntityManager */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    abstract protected function loadFromAPI();

    public function getDiscountsFromAPI()
    {
        $APIResponse = $this->loadFromAPI();
        $discountObjects = [];
        foreach ($APIResponse as $apiDiscount) {
            $discount = $this->getDiscountFromAPIData($apiDiscount);
            $discountObjects[] = $discount;
        }
        return $discountObjects;
    }

    protected function getDiscountFromAPIData($apiDiscount) : Discount
    {
        $discount = $this->getDiscountRepository()->findOneBy(['APIId' => $apiDiscount['id']]);
        if (!$discount) {
            $discount = new Discount();
        }
        $discount->setDateFound(new \DateTime());
        return $this->updateDiscountFromAPIData($discount, $apiDiscount);
    }

    protected function updateDiscountFromAPIData(Discount $discount, $apiDiscount) : Discount
    {
        $discount->setCode($apiDiscount['code']);
        $discount->setProgramId($apiDiscount['programId']);
        $discount->setValidFromDate(new \DateTime($apiDiscount['startDate']));
        $discount->setExpireDate(new \DateTime($apiDiscount['expiryDate']));
        $discount->setDescription($apiDiscount['description']);
        $discount->setURL($apiDiscount['destinationUrl']);
        $discount->setDiscountTitle($apiDiscount['discount']);
        $discount->setValue($apiDiscount['discount']);
        $discount->setShop($apiDiscount['program_name']);
        $discount->setAPIId($apiDiscount['id']);
        return $discount;
    }

    /**
     * @return DiscountRepository|EntityRepository
     */
    protected function getDiscountRepository(): DiscountRepository
    {
        return $this->getEm()->getRepository(Discount::class);
    }

    /**
     * @return EntityManager
     */
    protected function getEm(): EntityManager
    {
        return $this->em;
    }

}