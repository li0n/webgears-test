<?php
namespace App\Service;

use App\Interfaces\VouchersAPIInterface;

class VouchersAPIService implements VouchersAPIInterface
{
    private $isFirstFileLoaded = false;

    public function getDiscounts()
    {
        $APIDirectory = dirname(__DIR__ . '/../../../') .'/API/';
        if ($this->isFirstFileLoaded()) {
            $JSONFileContents = file_get_contents($APIDirectory . 'input2.json');
        } else {
            $JSONFileContents = file_get_contents($APIDirectory . 'input1.json');
        }
        return json_decode($JSONFileContents, true);
    }

    public function setIsLoaded($isFirstFileLoaded)
    {
        $this->isFirstFileLoaded = $isFirstFileLoaded;
    }

    /**
     * @return bool
     */
    private function isFirstFileLoaded(): bool
    {
        return $this->isFirstFileLoaded;
    }



}