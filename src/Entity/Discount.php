<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DiscountRepository")
 */
class Discount
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $programId;

    /**
     * @ORM\Column(type="integer")
     */
    private $APIId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $discountTitle;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $shop;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $value;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $URL;

    /**
     * @ORM\Column(type="datetime")
     */
    private $validFromDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $expireDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateFound;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateHidden;

    /**
     * @return mixed
     */
    public function getDateHidden()
    {
        return $this->dateHidden;
    }

    /**
     * @param mixed $dateHidden
     */
    public function setDateHidden($dateHidden): void
    {
        $this->dateHidden = $dateHidden;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getShop(): ?string
    {
        return $this->shop;
    }

    public function setShop(string $shop): self
    {
        $this->shop = $shop;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getURL(): ?string
    {
        return $this->URL;
    }

    public function setURL(string $URL): self
    {
        $this->URL = $URL;

        return $this;
    }

    public function getValidFromDate(): ?\DateTimeInterface
    {
        return $this->validFromDate;
    }

    public function setValidFromDate(\DateTimeInterface $validFromDate): self
    {
        $this->validFromDate = $validFromDate;

        return $this;
    }

    public function getExpireDate(): ?\DateTimeInterface
    {
        return $this->expireDate;
    }

    public function setExpireDate(?\DateTimeInterface $expireDate): self
    {
        $this->expireDate = $expireDate;

        return $this;
    }

    public function getDateFound(): ?\DateTimeInterface
    {
        return $this->dateFound;
    }

    public function setDateFound(\DateTimeInterface $dateFound): self
    {
        $this->dateFound = $dateFound;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProgramId()
    {
        return $this->programId;
    }

    /**
     * @param mixed $programId
     */
    public function setProgramId($programId): void
    {
        $this->programId = $programId;
    }

    /**
     * @return mixed
     */
    public function getAPIId()
    {
        return $this->APIId;
    }

    /**
     * @param mixed $APIId
     */
    public function setAPIId($APIId): void
    {
        $this->APIId = $APIId;
    }

    /**
     * @return mixed
     */
    public function getDiscountTitle()
    {
        return $this->discountTitle;
    }

    /**
     * @param mixed $discountTitle
     */
    public function setDiscountTitle($discountTitle): void
    {
        $this->discountTitle = $discountTitle;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    public function isExpired()
    {
        return $this->getExpireDate() < new \DateTime();
    }

    public function isVisible()
    {
        return $this->getDateHidden() === null ? true : $this->getDateFound() > $this->getDateHidden();
    }


}
