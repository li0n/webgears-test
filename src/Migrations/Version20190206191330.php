<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190206191330 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE discount (
            id INT AUTO_INCREMENT NOT NULL, 
            program_id INT NOT NULL, 
            apiid INT NOT NULL, 
            discount_title VARCHAR(255) NOT NULL, 
            description VARCHAR(255) NOT NULL, 
            shop VARCHAR(255) NOT NULL, 
            code VARCHAR(255) NOT NULL, 
            value VARCHAR(255) NOT NULL, 
            url VARCHAR(255) NOT NULL, 
            valid_from_date DATETIME NOT NULL, 
            expire_date DATETIME DEFAULT NULL, 
            date_found DATETIME NOT NULL, PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
    }


    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE discount');
    }
}
