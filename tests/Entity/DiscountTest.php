<?php
namespace App\Test\Entity;


use App\Entity\Discount;
use PHPUnit\Framework\TestCase;

class DiscountTest extends TestCase
{

    public function testIsExpiredWithOldExpireDate()
    {
        $discount = new Discount();
        $discount->setExpireDate(new \DateTime("2019-01-01")); // old date
        $this->assertTrue($discount->isExpired());
    }

    public function testIsNotExpiredWithFutureExpireDate()
    {
        $discount = new Discount();
        $discount->setExpireDate(new \DateTime("tomorrow")); // old date
        $this->assertFalse($discount->isExpired());
    }

    public function testIsNotVisibleWhenFoundDateIsEqualToHiddenDate()
    {
        $discount = new Discount();
        $discount->setDateFound(new \DateTime("2019-01-01"));
        $discount->setDateHidden(new \DateTime("2019-01-01"));
        $this->assertFalse($discount->isVisible());
    }

    public function testIsNotVisibleWhenFoundDateIsOlderThanHiddenDate()
    {
        $discount = new Discount();
        $discount->setDateFound(new \DateTime("2019-01-01"));
        $discount->setDateHidden(new \DateTime("2019-01-02"));
        $this->assertFalse($discount->isVisible());
    }

    public function testIsVisibleWhenFoundDateIsNewerThanHiddenDate()
    {
        $discount = new Discount();
        $discount->setDateFound(new \DateTime("2019-01-02"));
        $discount->setDateHidden(new \DateTime("2019-01-01"));
        $this->assertTrue($discount->isVisible());
    }

    public function testIsVisibleWhenHiddenDateIsNull()
    {
        $discount = new Discount();
        $discount->setDateFound(new \DateTime("2019-01-02"));
        $discount->setDateHidden(null);
        $this->assertTrue($discount->isVisible());
    }


}
