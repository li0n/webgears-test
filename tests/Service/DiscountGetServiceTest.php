<?php
namespace App\Tests\Service;

use App\Entity\Discount;
use App\Repository\DiscountRepository;
use App\Service\DiscountGetService;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;

class DiscountGetServiceTest extends TestCase
{

    public function testIsDiscountCreatedFromAPI()
    {
        // Prepare

        $expectedDiscountAPIData = [
            "code" => "RECARO5",
            "programId" => 4274,
            "startDate" => "2015-05-18T00:00:00+01:00",
            "expiryDate" => "2015-06-14T23:59:59+01:00",
            "description" => "5% Off Recaro Products",
            "destinationUrl" => "http://www.babys-mart.co.uk/brands/Recaro",
            "discount" => "5% Off Recaro Products",
            "program_name" => "Babys Mart",
            "currency" => "GBP",
            "id" => 13170,
            "commissionValueFormatted" => "Default",
        ];
        $expectedDiscount = $this->createDiscountFromArray($expectedDiscountAPIData);

        $serviceMock = $this->getDiscountServiceMock([
            $expectedDiscountAPIData,
        ], []);

        // Execute

        $discounts = $serviceMock->getDiscountsFromAPI();
        /** @var Discount $discount */
        $discount = current($discounts);

        // Test

        $this->assertEquals(1, count($discounts), "Expecting only one discount entity");
        $this->assertNull($discount->getId());
        $this->assertEquals($expectedDiscount->getCode(), $discount->getCode());
        $this->assertEquals($expectedDiscount->getProgramId(), $discount->getProgramId());
        $this->assertEquals($expectedDiscount->getValidFromDate(), $discount->getValidFromDate());
        $this->assertEquals($expectedDiscount->getExpireDate(), $discount->getExpireDate());
        $this->assertEquals($expectedDiscount->getDescription(), $discount->getDescription());
        $this->assertEquals($expectedDiscount->getURL(), $discount->getURL());
        $this->assertEquals($expectedDiscount->getDiscountTitle(), $discount->getDiscountTitle());
        $this->assertEquals($expectedDiscount->getShop(), $discount->getShop());
        $this->assertEquals($expectedDiscount->getAPIId(), $discount->getAPIId());
        $this->assertNotNull($discount->getDateFound());
    }

    public function testIsDiscountUpdatedFromAPI()
    {
        // Prepare

        $expectedDiscountAPIData = [
            "code" => "RECARO5",
            "programId" => 4274,
            "startDate" => "2015-05-18T00:00:00+01:00",
            "expiryDate" => "2015-06-14T23:59:59+01:00",
            "description" => "5% Off Recaro Products",
            "destinationUrl" => "http://www.babys-mart.co.uk/brands/Recaro",
            "discount" => "5% Off Recaro Products",
            "program_name" => "Babys Mart",
            "currency" => "GBP",
            "id" => 13170,
            "commissionValueFormatted" => "Default",
        ];

        $existingDiscount = $this->createDiscountFromArray($expectedDiscountAPIData, 'OLD');
        $existingDiscount->setDateFound(new \DateTime("2015-05-18T00:00:00+01:00"));
        $expectedDiscount = $this->createDiscountFromArray($expectedDiscountAPIData);
        $expectedDiscount->setDateFound(new \DateTime());
        $serviceMock = $this->getDiscountServiceMock([
            $expectedDiscountAPIData,
        ], [$existingDiscount]);


        // Execute

        $discounts = $serviceMock->getDiscountsFromAPI();
        /** @var Discount $discount */
        $discount = current($discounts);

        // Test

        $this->assertEquals(1, count($discounts), "Expecting only one discount entity");

        $this->assertNull($discount->getId());
        $this->assertEquals($expectedDiscount->getCode(), $discount->getCode());
        $this->assertEquals($expectedDiscount->getProgramId(), $discount->getProgramId());
        $this->assertEquals($expectedDiscount->getValidFromDate(), $discount->getValidFromDate());
        $this->assertEquals($expectedDiscount->getExpireDate(), $discount->getExpireDate());
        $this->assertEquals($expectedDiscount->getDescription(), $discount->getDescription());
        $this->assertEquals($expectedDiscount->getURL(), $discount->getURL());
        $this->assertEquals($expectedDiscount->getDiscountTitle(), $discount->getDiscountTitle());
        $this->assertEquals($expectedDiscount->getShop(), $discount->getShop());
        $this->assertEquals($expectedDiscount->getAPIId(), $discount->getAPIId());
        $this->assertEquals($expectedDiscount->getDateFound()->format("Y-m-d H:i"), $discount->getDateFound()->format("Y-m-d H:i"));
    }

    private function getEntityManagerMock($discountSearchResults)
    {
        $discountRepository = $this->createMock(DiscountRepository::class);
        $discountRepository->expects($this->any())
            ->method('find')
            ->willReturn($discountSearchResults);
        $discountRepository->expects($this->any())
            ->method('findBy')
            ->willReturn($discountSearchResults);
        $discountRepository->expects($this->any())
            ->method('findOneBy')
            ->willReturn(current($discountSearchResults));

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())->method('getRepository')->willReturn($discountRepository);

        return $em;
    }

    /**
     * @param $APIData
     * @param $discountSearchResults
     * @return \PHPUnit\Framework\MockObject\MockObject|DiscountGetService
     */
    private function getDiscountServiceMock($APIData, $discountSearchResults)
    {

        $service = $this->getMockBuilder(DiscountGetService::class)
            ->setMethods(['loadFromAPI'])
            ->setConstructorArgs([
                $this->getEntityManagerMock($discountSearchResults),
                "DUMB_API_URL",
                "/tmp/DUMB_COOKIE_FILE"
            ])
            ->getMock();

        $service
            ->expects($this->any())
            ->method('loadFromAPI')
            ->willReturn($APIData)
        ;
        return $service;
    }

    /**
     * @param array $discountArray
     * @param string $prefix
     * @return Discount
     * @throws \Exception
     */
    private function createDiscountFromArray(array $discountArray, string $prefix = ""): Discount
    {
        $existingDiscount = new Discount();
        $existingDiscount->setCode($prefix . $discountArray['code']);
        $existingDiscount->setProgramId($discountArray['programId']);
        $existingDiscount->setValidFromDate(new \DateTime($discountArray['startDate']));
        $existingDiscount->setExpireDate(new \DateTime($discountArray['expiryDate']));
        $existingDiscount->setDescription($prefix . $discountArray['description']);
        $existingDiscount->setURL($prefix . $discountArray['destinationUrl']);
        $existingDiscount->setDiscountTitle($prefix . $discountArray['discount']);
        $existingDiscount->setShop($prefix . $discountArray['program_name']);
        $existingDiscount->setAPIId($discountArray['id']);
        return $existingDiscount;
    }


}
