require('../css/app.scss');
const $ = require('jquery');
require('bootstrap');


$(document).ready(function() {
    $('[data-toggle="popover"]').popover();

    $('#dynamicContent').on("click", '.hide-discount', function(event) {
        var discount = $(this).parents('.discount'),
            discountHideURL = discount.find('.card-footer a').attr('href');
        $.getJSON(discountHideURL, function(data) {
            if (data == 'OK') {
                discount.remove();
            } else {
                discount.popover({content: data});
                discount.popover('show');
                setTimeout(function() { discount.popover('hide');}, 3000);
            }
            updateDiscountsAmount();
        });
        event.preventDefault();
    });

    $('#btnUpdate').on('click', function (event) {
        event.preventDefault();
        $.get($(this).attr("href"), function(data) {
            $('#dynamicContent').html(data);
            updateDiscountsAmount();
        });
    });

    function updateDiscountsAmount() {
        var label = $('#discountsAmount'),
            list = $('#discountsList');
        label.text($('.discount', list).length);
    }
});